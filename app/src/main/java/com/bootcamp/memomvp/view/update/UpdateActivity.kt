package com.bootcamp.memomvp.view.update

import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bootcamp.memomvp.R
import com.bootcamp.memomvp.data.model.TaskEntity
import com.bootcamp.memomvp.data.source.TaskRepository
import com.bootcamp.memomvp.data.source.TaskRepositoryInterface
import com.bootcamp.memomvp.view.task.TaskActivity
import kotlinx.android.synthetic.main.activity_edit.*
import kotlinx.android.synthetic.main.activity_main.*

class UpdateActivity : AppCompatActivity(), UpdateView {
    lateinit var taskRepositoryInterface: TaskRepositoryInterface
    lateinit var updatePresenter: UpdatePresenter
    val Task : TaskEntity? by lazy{
        intent.getParcelableExtra<TaskEntity>("Task")
    }

    override fun onSuccessEdit(result: String) {
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show()
        edtxtTaskTitleEdit.setText("")
        edtxDescriptionEdit.setText("")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)
        taskRepositoryInterface = TaskRepository(this)
        updatePresenter = UpdatePresenterImplement(this, taskRepositoryInterface)
        Task?.let {
            edtxtTaskTitleEdit.setText(it.title)
            edtxDescriptionEdit.setText(it.description)
        }
        setSupportActionBar(toolbarEdit)
        val listEdit : View = listFloatingEdit
        listEdit.setOnClickListener { view ->
            val i = Intent(this, TaskActivity::class.java)
            startActivity(i)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater : MenuInflater = menuInflater
        inflater.inflate(R.menu.item_task, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.menuSave -> {
                val title = edtxtTaskTitleEdit.text.toString()
                val description = edtxDescriptionEdit.text.toString()

                updatePresenter.updateTask(TaskEntity(Task?.id, title, description))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}