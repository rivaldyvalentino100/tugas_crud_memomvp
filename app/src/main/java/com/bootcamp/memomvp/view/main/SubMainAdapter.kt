package com.bootcamp.memomvp.view.main

import android.app.AlertDialog
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bootcamp.memomvp.R
import com.bootcamp.memomvp.data.model.TaskEntity
import com.bootcamp.memomvp.view.update.UpdateActivity
import kotlinx.android.synthetic.main.list_view_task.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class SubMainAdapter (val listTask : List<TaskEntity>, val subMainPresenter: SubMainPresenter) : RecyclerView.Adapter<SubMainAdapter.ListTaskViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListTaskViewHolder {
        return ListTaskViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_view_task, parent, false))
    }

    override fun onBindViewHolder(holder: ListTaskViewHolder, position: Int) {
        listTask.get(position).let {
            holder.title.text = it.title
            holder.description.text = it.description
        }

        holder.itemView.BtnEdit.setOnClickListener {
            val intentEditActivity = Intent(it.context, UpdateActivity::class.java)

            intentEditActivity.putExtra("Task", listTask[position])
            it.context.startActivity((intentEditActivity))
        }

        holder.itemView.BtnDelete.setOnClickListener{
            AlertDialog.Builder(it.context).setPositiveButton("Ya"){p0, p1 ->
                subMainPresenter.deleteTask(listTask.get(position))
            }.setNegativeButton("Tidak"){ p0, p1 ->
                p0.dismiss()
            }.setMessage("Apakah anda yakin menghapus data ${listTask[position].id}").setTitle("Konfirmasi Hapus").create().show()
        }
    }

    override fun getItemCount(): Int {
        return listTask.size
    }

    inner class ListTaskViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val title = view.tvTitle
        val description = view.tvDescription
    }
}