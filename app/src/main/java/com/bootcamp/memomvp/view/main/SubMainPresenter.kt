package com.bootcamp.memomvp.view.main

import com.bootcamp.memomvp.data.model.TaskEntity

interface SubMainPresenter {
    fun getAllTask()

    fun deleteTask(taskEntity: TaskEntity)
}