package com.bootcamp.memomvp.view.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bootcamp.memomvp.R
import com.bootcamp.memomvp.data.model.TaskEntity
import com.bootcamp.memomvp.data.source.TaskRepository
import com.bootcamp.memomvp.data.source.TaskRepositoryInterface
import com.bootcamp.memomvp.view.task.TaskActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_task.*
import kotlinx.android.synthetic.main.list_view_task.*
import kotlinx.android.synthetic.main.list_view_task.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class SubMainActivity : AppCompatActivity(), SubMainView {
    lateinit var taskRepositoryInterface: TaskRepositoryInterface
    lateinit var subMainPresenter: SubMainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        taskRepositoryInterface = TaskRepository(this)
        subMainPresenter = SubMainPresenterImplement(this, taskRepositoryInterface)
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        subMainPresenter.getAllTask()
        swipeRefresh.setOnRefreshListener{
            subMainPresenter.getAllTask()
            swipeRefresh.isRefreshing = false
        }
        val add : View = addFloating
        add.setOnClickListener { view ->
            val i = Intent(this, TaskActivity::class.java)
            startActivity(i)
        }
    }

    override fun getAllTask(list: List<TaskEntity>?) {
        GlobalScope.launch {
            runOnUiThread {
                list?.let{
                    val adapter = SubMainAdapter(it, subMainPresenter)
                    recyclerView.adapter = adapter
                }
            }
        }
    }

    override fun onDelete(result: String) {
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show()
    }
}