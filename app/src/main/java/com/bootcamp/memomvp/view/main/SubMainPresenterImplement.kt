package com.bootcamp.memomvp.view.main

import com.bootcamp.memomvp.data.model.TaskEntity
import com.bootcamp.memomvp.data.source.TaskRepositoryInterface

class SubMainPresenterImplement(private val subMainView: SubMainView, private val taskRepositoryInterface: TaskRepositoryInterface) : SubMainPresenter {
    override fun getAllTask() {
        val result = taskRepositoryInterface.getAllTask()

        subMainView.getAllTask(result)
    }

    override fun deleteTask(taskEntity: TaskEntity) {
        val result = taskRepositoryInterface.deleteTask(taskEntity)

        if (result != 0){
            subMainView.onDelete("Delete Success")
        } else {
            subMainView.onDelete("Delete Failed")
        }
    }

}