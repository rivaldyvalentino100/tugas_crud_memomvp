package com.bootcamp.memomvp.view.main

import com.bootcamp.memomvp.data.model.TaskEntity

interface SubMainView {
    fun getAllTask(list: List<TaskEntity>?)

    fun onDelete(result : String)
}